
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) throws Exception {
        subTask1(4, 10);
        subTask2(new int[] { 1, 2, 3, 100, 10 }, new int[] { 3, 100, 10, 7, 8 });
        subTask3(2);
        subTask4(new int[] { 1, 2, 10 });
        subTask5(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        subTask6(new int[] { 1, 0, 2, 3, 4 }, new int[] { 3, 5, 6, 7, 8, 13 });
        subTask7(new int[] { 1, 2, 3, 1, 5, 1, 4, 6, 3, 4 });
        subTask8(new int[] { 1, 2, 3 }, new int[] { 100, 2, 1, 10 });
        subTask9(new Integer[] { 100, 2, 1, 10 });
        subTask10(1, 2);
    }

    public static void subTask1(int x, int y) {
        ArrayList<Integer> array = new ArrayList<>();

        for (int i = x; i <= y; i++) {
            array.add(i);
        }
        System.out.print(array);
    }

    public static void subTask2(int[] array1, int[] array2) {
        Map<Integer, Integer> hashMap = new HashMap<>();
        ArrayList<Integer> uniqueList = new ArrayList<>();
        for (int ele1 : array1) {
            hashMap.put(ele1, 1);
            uniqueList.add(ele1);
        }

        for (int ele2 : array2) {
            if (!hashMap.containsKey(ele2)) {
                hashMap.put(ele2, 1);
                uniqueList.add(ele2);
            }
        }
        System.out.println(uniqueList);
    }

    public static void subTask3(int n) {
        int count = 0;
        int[] newArray = { 1, 2, 1, 4, 5, 1, 1, 3, 1 };
        for (int ele : newArray) {
            if (ele == n) {
                count++;
            }
        }
        System.out.println(count);
    }

    public static void subTask4(int[] array) {
        int sum = 0;
        for (int ele : array) {
            sum += ele;
        }
        System.out.println(sum);
    }

    public static void subTask5(int[] array) {
        ArrayList<Integer> evenArray = new ArrayList<>();
        for (int ele : array) {
            if (ele % 2 == 0) {
                evenArray.add(ele);
            }
        }
        System.out.println(evenArray);
    }

    public static void subTask6(int[] array1, int[] array2) {
        ArrayList<Integer> newArray = new ArrayList<>();
        int minLength = Math.min(array1.length, array2.length);
        for (int i = 0; i < minLength; i++) {
            int ele1 = array1[i];
            int ele2 = array2[i];
            int sum = ele1 + ele2;
            newArray.add(sum);

        }

        if (array2.length > array1.length) {
            for (int i = array1.length; i < array2.length; i++) {
                newArray.add(array2[i]);
            }
        }
        System.out.println(newArray);
    }

    public static void subTask7(int[] array) {
        Map<Integer, Integer> hashMap = new HashMap<>();
        ArrayList<Integer> uniqueList = new ArrayList<>();

        for (Integer ele : array) {
            if (!hashMap.containsKey(ele)) {
                hashMap.put(ele, 1);
                uniqueList.add(ele);
            }
        }
        System.out.println(uniqueList);
    }

    public static void subTask8(int[] array1, int[] array2) {
        Map<Integer, Integer> hashMap = new HashMap<>();
        ArrayList<Integer> uniqueList = new ArrayList<>();
        int count = 1;
        for (int ele1 : array1) {
            hashMap.put(ele1, count);
        }
        for (int ele2 : array2) {
            if (hashMap.containsKey(ele2)) {
                hashMap.put(ele2, count++);
            } else {
                hashMap.put(ele2, count);
            }
        }

        for (Map.Entry<Integer, Integer> entry : hashMap.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if (value == 1) {
                uniqueList.add(key);
            }
        }
        System.out.println(uniqueList);
    }

    public static void subTask9(Integer[] array) {
        Arrays.sort(array, Collections.reverseOrder());
        for (Integer arr : array) {
            System.out.println(arr);
        }
    }

    public static void subTask10(int x, int y) {
        int[] array = { 10, 20, 30, 40, 50 };
        int temp = array[x];
        array[x] = array[y];
        array[y] = temp;

        for (int i : array) {
            System.out.println(i);
        }
    }

}
